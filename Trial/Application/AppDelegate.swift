//
//  AppDelegate.swift
//  Trial
//
//  Created by Stefan Rinner on 12.02.18.
//  Copyright © 2018 BeeOne Gmbh. All rights reserved.
//

import UIKit
import SwiftUI
import Resolver

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]? = nil) -> Bool {

        DependencyManager.registerDependencies()

        #if !TEST
        setupWindow()
        #endif
        
        return true
    }

    private func setupWindow() {
        let navigator: Navigator = Resolver.resolve()
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.rootViewController = UIHostingController(rootView: navigator.root())
        window?.makeKeyAndVisible()
    }
}

