//
//  DependencyManager.swift
//  Trial
//
//  Created by Kristof Varga on 2021. 09. 05..
//  Copyright © 2021. BeeOne Gmbh. All rights reserved.
//

import Foundation
import Resolver

class DependencyManager {
    static func registerDependencies() {
        registerServices()
        registerRepositories()
        registerUseCases()
        registerNavigators()
    }

    private static func registerServices() {
        Resolver.register(TransactionService.self) {
            TransactionsAPI()
        }
    }

    private static func registerRepositories() {
        Resolver.register(TransactionRepository.self) {
            DefaultTransactionRepository(transactionService: Resolver.resolve())
        }
        .scope(.cached)

        Resolver.register(UserPreferencesRepository.self) {
            UserDefaults.standard
        }
        .scope(.application)
    }

    private static func registerUseCases() {
        Resolver.register(FetchTransactionsUseCase.self) {
            DefaultFetchTransactionsUseCase(transactionRepository: Resolver.resolve())
        }

        Resolver.register(FetchTransactionByIdUseCase.self) {
            DefaultFetchTransactionByIdUseCase(transactionRepository: Resolver.resolve())
        }

        Resolver.register(SumTransactionsByCurrencyUseCase.self) {
            DefaultSumTransactionsByCurrencyUseCase()
        }

        Resolver.register(FirstLaunchUseCase.self) {
            DefaultFirstLaunchUseCase(userPreferences: Resolver.resolve())
        }
    }

    private static func registerNavigators() {
        Resolver.register(Navigator.self) {
            FeatureNavigator()
        }
        .implements(TransactionListNavigator.self)
        .scope(.cached)
    }
}
