//
//  FeatureFactory.swift
//  Trial
//
//  Created by Kristof Varga on 2021. 09. 05..
//  Copyright © 2021. BeeOne Gmbh. All rights reserved.
//

import Foundation
import SwiftUI
import Resolver

final class FeatureNavigator: Navigator {

    // MARK: - Navigator

    func root() -> AnyView {
        transactionList()
    }

    // MARK: - TransactionListNavigator

    func transactionDetail(id: String) -> AnyView {
        let viewModel = TransactionDetailViewModel(transactionId: id, fetchUseCase: Resolver.resolve())
        return AnyView(ViewModelWrapper(contentView: SwiftUITransactionDetailView(), vm: viewModel))
    }

    func splash() -> AnyView {
        AnyView(SplashView())
    }

    // MARK: - Private

    private func transactionList() -> AnyView {
        let viewModel = TransactionListViewModel(fetchTransactionsUseCase: Resolver.resolve(), sumTransactionsUseCase: Resolver.resolve(), firstLaunchUseCase: Resolver.resolve())
        return AnyView(SwiftUITransactionListView(viewModel: viewModel, navigator: Resolver.resolve()))
    }
}

protocol Navigator: TransactionListNavigator {
    func root() -> AnyView
}
