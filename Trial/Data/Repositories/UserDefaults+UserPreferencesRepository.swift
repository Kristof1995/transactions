//
//  UserDefaults+UserPreferencesRepository.swift
//  Trial
//
//  Created by Kristof Varga on 2021. 09. 06..
//  Copyright © 2021. BeeOne Gmbh. All rights reserved.
//

import Foundation

extension UserDefaults: UserPreferencesRepository {
    func getValue<T>(for key: String) -> T? {
        value(forKey: key) as? T
    }

    func save<T>(_ value: T, for key: String) {
        setValue(value, forKey: key)
    }
}
