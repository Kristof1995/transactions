//
//  DefaultTransactionRepository.swift
//  Trial
//
//  Created by Kristof Varga on 2021. 09. 02..
//  Copyright © 2021. BeeOne Gmbh. All rights reserved.
//

import Foundation

protocol TransactionService {
    func fetchTransactionDTOs(completion: @escaping (Result<[TransactionDTO], Error>) -> Void)
}

final class DefaultTransactionRepository {

    private let transactionService: TransactionService

    private var memCache: [Transaction]?

    init(transactionService: TransactionService) {
        self.transactionService = transactionService
    }
}

extension DefaultTransactionRepository: TransactionRepository {
    func fetchTransactionList(cachePolicy: CachePolicy, completion: @escaping (Result<[Transaction], Error>) -> Void) {

        if let memCache = memCache, cachePolicy == .useCache {
            completion(.success(memCache))
            return
        }

        transactionService.fetchTransactionDTOs { [weak self] result in
            switch result {
            case .success(let dtos):
                let models = dtos.map { $0.toDomain() }
                self?.memCache = models
                completion(.success(models))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
}
