//
//  TransactionsAPI+TransactionService.swift
//  Trial
//
//  Created by Kristof Varga on 2021. 09. 05..
//  Copyright © 2021. BeeOne Gmbh. All rights reserved.
//

import Foundation

extension TransactionsAPI: TransactionService {
    func fetchTransactionDTOs(completion: @escaping (Result<[TransactionDTO], Error>) -> Void) {
        loadTransactions { result in
            switch result {
            case .success(let transactions):
                completion(.success(transactions))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
}
