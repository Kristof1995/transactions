//
//  MockSumTransactionsUseCase.swift
//  Trial
//
//  Created by Kristof Varga on 2021. 09. 04..
//  Copyright © 2021. BeeOne Gmbh. All rights reserved.
//

import Foundation

class MockSumTransactionsUseCase: SumTransactionsByCurrencyUseCase {

    var sumAmounts: [Transaction.Amount]?

    func calculeteSumGroupedByCurrency(transactions: [Transaction]) -> [Transaction.Amount] {
        sumAmounts ?? []
    }
}
