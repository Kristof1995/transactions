//
//  MockFetchTransactionByIdUseCase.swift
//  Trial
//
//  Created by Kristof Varga on 2021. 09. 05..
//  Copyright © 2021. BeeOne Gmbh. All rights reserved.
//

import Foundation

class MockFetchTransactionByIdUseCase: FetchTransactionByIdUseCase {
    var transaction: Transaction?
    var error: Error?

    func fetch(transactionId: String, cachePolicy: CachePolicy, completion: @escaping (Result<Transaction?, Error>) -> Void) {

        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            if let error = self.error {
                completion(.failure(error))
            } else {
                completion(.success(self.transaction))
            }
        }
    }
}
