//
//  MockFetchTransactionsUseCase.swift
//  Trial
//
//  Created by Kristof Varga on 2021. 09. 04..
//  Copyright © 2021. BeeOne Gmbh. All rights reserved.
//

import Foundation

class MockFetchTransactionsUseCase: FetchTransactionsUseCase {
    var transactions: [Transaction]?
    var error: Error?

    func fetch(cachePolicy: CachePolicy, completion: @escaping (Result<[Transaction], Error>) -> Void) {

        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            if let transactions = self.transactions {
                completion(.success(transactions))
            } else if let error = self.error {
                completion(.failure(error))
            }
        }
    }
}
