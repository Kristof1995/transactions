//
//  MockFirstLaunchUseCase.swift
//  Trial
//
//  Created by Kristof Varga on 2021. 09. 06..
//  Copyright © 2021. BeeOne Gmbh. All rights reserved.
//

import Foundation

class MockFirstLaunchUseCase: FirstLaunchUseCase {

    var isFirst = true

    func isFirstLaunch() -> Bool {
        isFirst
    }

    func saveFirtsLaunch() {
        isFirst = false
    }
}
