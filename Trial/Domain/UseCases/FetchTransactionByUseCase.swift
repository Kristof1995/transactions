//
//  FetchTransactionByUseCase.swift
//  Trial
//
//  Created by Kristof Varga on 2021. 09. 02..
//  Copyright © 2021. BeeOne Gmbh. All rights reserved.
//

import Foundation

protocol FetchTransactionByIdUseCase {
    func fetch(transactionId: String, cachePolicy: CachePolicy, completion: @escaping (Result<Transaction?, Error>) -> Void)
}

final class DefaultFetchTransactionByIdUseCase: FetchTransactionByIdUseCase {

    private let transactionRepository: TransactionRepository

    init(transactionRepository: TransactionRepository) {
        self.transactionRepository = transactionRepository
    }

    func fetch(transactionId: String, cachePolicy: CachePolicy, completion: @escaping (Result<Transaction?, Error>) -> Void) {
        transactionRepository.fetchTransactionList(cachePolicy: cachePolicy) { [weak self] result in
            guard let self = self else { return }

            switch result {
            case .success(let transcations):
                self.search(id: transactionId, in: transcations, completion: completion)
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }

    private func search(id: String, in transactions: [Transaction], completion: @escaping (Result<Transaction?, Error>) -> Void) {
        completion(.success(transactions.first(where: { $0.id == id })))
    }
}
