//
//  SumTransactionsByCurrencyUseCase.swift
//  Trial
//
//  Created by Kristof Varga on 2021. 09. 02..
//  Copyright © 2021. BeeOne Gmbh. All rights reserved.
//

import Foundation

protocol SumTransactionsByCurrencyUseCase {
    func calculeteSumGroupedByCurrency(transactions: [Transaction]) -> [Transaction.Amount]
}

final class DefaultSumTransactionsByCurrencyUseCase: SumTransactionsByCurrencyUseCase {
    func calculeteSumGroupedByCurrency(transactions: [Transaction]) -> [Transaction.Amount] {
        let amountsByCurrency = groupAmountsByCurrency(from: transactions)
        return calculateSum(of: amountsByCurrency)
    }

    private func groupAmountsByCurrency(from transactions: [Transaction]) -> [String: [Transaction.Amount]] {
        Dictionary(grouping: transactions.map { $0.amount }) { amount in
            amount.currency
        }
    }

    private func calculateSum(of dictionary: [String: [Transaction.Amount]]) -> [Transaction.Amount] {
        dictionary
            .sorted(by: { $0.key < $1.key })
            .map { (currency, amounts) -> Transaction.Amount in
                let values = amounts.map { $0.value }
                let sumValue = values.reduce(NSDecimalNumber.zero, +)
                return Transaction.Amount(value: sumValue, currency: currency)
            }
    }
}

func +(lhs: NSDecimalNumber, rhs: NSDecimalNumber) -> NSDecimalNumber {
    return lhs.adding(rhs)
}
