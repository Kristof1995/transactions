//
//  FetchTransactionsUseCase.swift
//  Trial
//
//  Created by Kristof Varga on 2021. 09. 02..
//  Copyright © 2021. BeeOne Gmbh. All rights reserved.
//

import Foundation

protocol FetchTransactionsUseCase {
    func fetch(cachePolicy: CachePolicy, completion: @escaping (Result<[Transaction], Error>) -> Void)
}

final class DefaultFetchTransactionsUseCase: FetchTransactionsUseCase {

    private let transactionRepository: TransactionRepository

    init(transactionRepository: TransactionRepository) {
        self.transactionRepository = transactionRepository
    }

    func fetch(cachePolicy: CachePolicy, completion: @escaping (Result<[Transaction], Error>) -> Void) {
        transactionRepository.fetchTransactionList(cachePolicy: cachePolicy, completion: completion)
    }
}
