//
//  FirstLaunchUseCase.swift
//  Trial
//
//  Created by Kristof Varga on 2021. 09. 06..
//  Copyright © 2021. BeeOne Gmbh. All rights reserved.
//

import Foundation

protocol FirstLaunchUseCase {
    func isFirstLaunch() -> Bool
    func saveFirtsLaunch()
}

class DefaultFirstLaunchUseCase: FirstLaunchUseCase {

    private let isFirstLaunchKey = "isFirstLaunch"

    private let userPreferences: UserPreferencesRepository

    init(userPreferences: UserPreferencesRepository) {
        self.userPreferences = userPreferences
    }

    func isFirstLaunch() -> Bool {
        userPreferences.getValue(for: isFirstLaunchKey) ?? true
    }

    func saveFirtsLaunch() {
        userPreferences.save(false, for: isFirstLaunchKey)
    }
}
