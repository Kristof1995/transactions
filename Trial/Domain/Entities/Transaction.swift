//
//  Transaction.swift
//  Trial
//
//  Created by Kristof Varga on 2021. 09. 02..
//  Copyright © 2021. BeeOne Gmbh. All rights reserved.
//

import Foundation

struct Transaction: Identifiable, Equatable {
    let id: String
    let title: String
    let subtitle: String?
    let amount: Amount
    let additionalTexts: [String]

    struct Amount: Equatable {
        let value: NSDecimalNumber
        let currency: String
    }
}
