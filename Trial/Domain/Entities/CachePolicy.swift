//
//  CachePolicy.swift
//  Trial
//
//  Created by Kristof Varga on 2021. 09. 07..
//  Copyright © 2021. BeeOne Gmbh. All rights reserved.
//

import Foundation

enum CachePolicy {
    case ignore
    case useCache
}
