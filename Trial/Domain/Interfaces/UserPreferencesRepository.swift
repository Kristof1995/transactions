//
//  UserPreferencesRepository.swift
//  Trial
//
//  Created by Kristof Varga on 2021. 09. 06..
//  Copyright © 2021. BeeOne Gmbh. All rights reserved.
//

import Foundation

protocol UserPreferencesRepository {
    func getValue<T>(for key: String) -> T?
    func save<T>(_ value: T, for key: String)
}
