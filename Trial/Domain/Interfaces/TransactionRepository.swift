//
//  TransactionRepository.swift
//  Trial
//
//  Created by Kristof Varga on 2021. 09. 02..
//  Copyright © 2021. BeeOne Gmbh. All rights reserved.
//

import Foundation

protocol TransactionRepository {
    func fetchTransactionList(cachePolicy: CachePolicy, completion: @escaping (Result<[Transaction], Error>) -> Void)
}
