//
//  View+Extension.swift
//  Trial
//
//  Created by Kristof Varga on 2021. 09. 05..
//  Copyright © 2021. BeeOne Gmbh. All rights reserved.
//

import Foundation
import SwiftUI

extension View {
    func errorAlert(isPresented: Binding<Bool>, message: String?, retry: @escaping () -> Void) -> some View {
        self.alert(isPresented: isPresented, content: {
            Alert(title: Text("Error happened"), message: Text(message ?? ""), primaryButton: .cancel(Text("Ok")), secondaryButton: .default(Text("Retry"), action: {
                retry()
            }))
        })
    }
}
