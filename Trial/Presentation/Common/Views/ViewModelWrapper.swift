//
//  ViewModelWrapper.swift
//  Trial
//
//  Created by Kristof Varga on 2021. 09. 07..
//  Copyright © 2021. BeeOne Gmbh. All rights reserved.
//

import Foundation
import SwiftUI

// This code can replace @SateObject on iOS 13
struct ViewModelWrapper<V: View, ViewModel: ObservableObject>: View {
    private let contentView: V
    @State private var contentViewModel: ViewModel

    init(contentView: @autoclosure () -> V, vm: @autoclosure () -> ViewModel) {
        self._contentViewModel = State(initialValue: vm())
        self.contentView = contentView()
    }

    var body: some View {
        contentView
            .environmentObject(contentViewModel)
    }
}
