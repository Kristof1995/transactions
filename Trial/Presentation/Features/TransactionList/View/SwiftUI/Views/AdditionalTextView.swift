//
//  AdditionalTextView.swift
//  Trial
//
//  Created by Kristof Varga on 2021. 09. 05..
//  Copyright © 2021. BeeOne Gmbh. All rights reserved.
//

import Foundation
import SwiftUI

struct AddtionalTextView: View {
    let lines: [String]

    var body: some View {
        if !lines.isEmpty {
            VStack(alignment: .leading, spacing: 5) {
                Text("Additional text:")
                    .font(.subheadline)
                    .bold()
                    .padding(.top)
                ForEach(lines, id: \.self) { additionalText in
                    Text(additionalText)
                        .font(.caption)
                }
            }
        }
    }
}
