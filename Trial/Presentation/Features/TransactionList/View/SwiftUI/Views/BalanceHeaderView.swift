//
//  BalanceHeaderView.swift
//  Trial
//
//  Created by Kristof Varga on 2021. 09. 05..
//  Copyright © 2021. BeeOne Gmbh. All rights reserved.
//

import Foundation
import SwiftUI

struct BalanceHeaderView: View {

    let items: [String]

    var body: some View {
        ScrollView(.horizontal, showsIndicators: false) {
            HStack(spacing: 20) {
                ForEach(items, id: \.self) { item in
                    BalanceItemView(item: item)
                }
            }
            .padding()
        }
    }
}

private struct BalanceItemView: View {
    let item: String
    var body: some View {
        Text(item)
            .bold()
            .font(.title)
    }
}
