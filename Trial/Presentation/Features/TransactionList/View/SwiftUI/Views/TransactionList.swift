//
//  TransactionList.swift
//  Trial
//
//  Created by Kristof Varga on 2021. 09. 05..
//  Copyright © 2021. BeeOne Gmbh. All rights reserved.
//

import Foundation
import SwiftUI

struct TransactionList: View {
    let items: [TransactionCellViewContent]
    let navigator: TransactionListNavigator

    var body: some View {
        List(items) { item in
            NavigationLink(
                destination: navigator.transactionDetail(id: item.id),
                label: {
                    TransactionCell(transaction: item)
                })
                .isDetailLink(true)
        }
        .listStyle(PlainListStyle())
    }
}

private struct TransactionCell: View {
    let transaction: TransactionCellViewContent
    var body: some View {
        VStack(alignment: .leading) {
            HStack {
                VStack(alignment: .leading, spacing: 5) {
                    Text(transaction.title)
                        .font(.headline)
                    if let subtitle = transaction.subtitle {
                        Text(subtitle)
                            .font(.subheadline)
                    }
                }

                Spacer()

                Text(transaction.amount)
                    .bold()
            }

            AddtionalTextView(lines: transaction.additionalTexts)
        }
        .padding()
    }
}
