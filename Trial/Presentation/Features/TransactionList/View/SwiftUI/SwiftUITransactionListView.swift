//
//  SwiftUITransactionListView.swift
//  Trial
//
//  Created by Kristof Varga on 2021. 09. 03..
//  Copyright © 2021. BeeOne Gmbh. All rights reserved.
//

import SwiftUI
import Combine

protocol TransactionListNavigator {
    func transactionDetail(id: String) -> AnyView
    func splash() -> AnyView
}

struct SwiftUITransactionListView: View {
    @ObservedObject var viewModel: TransactionListViewModel
    @State private var isAlertPresented = false

    let navigator: TransactionListNavigator

    private var refreshButton: some View {
        Button(action: {
            viewModel.retry()
        }, label: {
            Image(systemName: "arrow.clockwise")
        })
    }

    var body: some View {
        NavigationView {
            ZStack {
                VStack(alignment: .leading) {
                    BalanceHeaderView(items: viewModel.balances)

                    TransactionList(items: viewModel.items, navigator: navigator)
                }

                if viewModel.isLoading {
                    ActivityIndicator(isAnimating: viewModel.isLoading)
                }
            }
            .onReceive(viewModel.$errorText, perform: { errorText in
                isAlertPresented = errorText != nil
            })
            .errorAlert(isPresented: $isAlertPresented, message: viewModel.errorText, retry: viewModel.retry)
            .navigationBarTitle(Text(viewModel.title), displayMode: .inline)
            .navigationBarItems(trailing: refreshButton)
        }
        .onAppear {
            viewModel.onAppear()
        }
        .sheet(isPresented: $viewModel.isSplashPresented, content: {
            navigator.splash()
                .onDisappear {
                    if viewModel.items.isEmpty {
                        viewModel.retry()
                    }
                }
        })
    }
}

struct SwiftUITransactionListView_Previews: PreviewProvider {

    static var previews: some View {

        class MockNavigator: TransactionListNavigator {
            func splash() -> AnyView {
                AnyView(Text("Splash"))
            }

            func transactionDetail(id: String) -> AnyView {
                AnyView(Text("id"))
            }
        }

        let fetchUseCase = MockFetchTransactionsUseCase()

        fetchUseCase.transactions = (0..<100).map {
            Transaction(id: "\($0)", title: "Title\($0)", subtitle: "Subtitle \($0)", amount: .init(value: NSDecimalNumber(value: $0), currency: "EUR"), additionalTexts: ["additional1", "additional2"])
        }

        let sumUseCase = MockSumTransactionsUseCase()
        sumUseCase.sumAmounts = [.init(value: .zero, currency: "EUR"), .init(value: NSDecimalNumber(value: 432.2), currency: "HUF")]

        let firstLaunchUseCase = MockFirstLaunchUseCase()
        firstLaunchUseCase.isFirst = false

        return SwiftUITransactionListView(viewModel: TransactionListViewModel(fetchTransactionsUseCase: fetchUseCase, sumTransactionsUseCase: sumUseCase, firstLaunchUseCase: firstLaunchUseCase), navigator: MockNavigator())
    }
}
