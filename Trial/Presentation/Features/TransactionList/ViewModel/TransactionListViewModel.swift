//
//  TransactionListViewModel.swift
//  Trial
//
//  Created by Kristof Varga on 2021. 09. 03..
//  Copyright © 2021. BeeOne Gmbh. All rights reserved.
//

import Foundation
import Combine

struct TransactionCellViewContent: Identifiable {
    let id: String
    let title: String
    let subtitle: String?
    let amount: String
    let additionalTexts: [String]
}

final class TransactionListViewModel: ObservableObject {
    @Published var items: [TransactionCellViewContent] = []
    @Published var title: String = "Transactions"
    @Published var balanceTitle = "Balance"
    @Published var balances: [String] = []
    @Published var isLoading: Bool = false
    @Published var errorText: String?
    @Published var isSplashPresented = false

    private let numberFormatter = NumberFormatter()

    private let fetchTransactionsUseCase: FetchTransactionsUseCase
    private let sumTransactionsUseCase: SumTransactionsByCurrencyUseCase
    private let firstLaunchUseCase: FirstLaunchUseCase

    init(fetchTransactionsUseCase: FetchTransactionsUseCase, sumTransactionsUseCase: SumTransactionsByCurrencyUseCase, firstLaunchUseCase: FirstLaunchUseCase) {
        self.fetchTransactionsUseCase = fetchTransactionsUseCase
        self.sumTransactionsUseCase = sumTransactionsUseCase
        self.firstLaunchUseCase = firstLaunchUseCase
        setup()
    }

    // MARK: - Input

    func onAppear() {
        launch()
        fetch(isReload: false)
    }

    // MARK: - Private

    private func setup() {
        setupFormatter()
    }

    private func launch() {
        let isFirstLaunch = firstLaunchUseCase.isFirstLaunch()

        if isFirstLaunch {
            isSplashPresented = true
            firstLaunchUseCase.saveFirtsLaunch()
        }
    }

    private func setupFormatter() {
        numberFormatter.numberStyle = .currency
    }

    private func fetch(isReload: Bool) {
        isLoading = true

        let cachePolicy: CachePolicy = isReload ? .ignore : .useCache

        fetchTransactionsUseCase.fetch(cachePolicy: cachePolicy) { [weak self] result in
            guard let self = self else { return }
            DispatchQueue.main.async {
                self.isLoading = false
                
                switch result {
                case .success(let transactions):
                    self.handle(transactions)
                case .failure(let error):
                    self.handle(error)
                }
            }
        }
    }
    
    func retry() {
        fetch(isReload: true)
    }

    private func handle(_ transactions: [Transaction]) {
        errorText = nil
        items = convertTransactionsToViewContent(transactions)
        balances = balances(from: transactions)
    }

    private func handle(_ error: Error) {
        errorText = "Error happened: " + error.localizedDescription
    }
}

private extension TransactionListViewModel {
    func convertTransactionsToViewContent(_ transactions: [Transaction]) -> [TransactionCellViewContent] {
        transactions.map { self.convertTransactionToViewContent($0) }
    }

    func convertTransactionToViewContent(_ transaction: Transaction) -> TransactionCellViewContent {
        let amountText = formatAmount(transaction.amount, with: numberFormatter)

        return .init(id: transaction.id, title: transaction.title, subtitle: transaction.subtitle, amount: amountText, additionalTexts: transaction.additionalTexts)
    }

    func formatAmount(_ amount: Transaction.Amount, with formatter: NumberFormatter) -> String {
        formatter.currencyCode = amount.currency
        return formatter.string(from: amount.value) ?? ""
    }

    func balances(from transactions: [Transaction]) -> [String] {
        let sumAmounts = sumTransactionsUseCase.calculeteSumGroupedByCurrency(transactions: transactions)

        return sumAmounts.map { self.formatAmount($0, with: self.numberFormatter) }
    }
}
