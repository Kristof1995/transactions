//
//  TransactionDetailViewModel.swift
//  Trial
//
//  Created by Kristof Varga on 2021. 09. 05..
//  Copyright © 2021. BeeOne Gmbh. All rights reserved.
//

import Foundation
import Combine

class TransactionDetailViewModel: ObservableObject {

    @Published var title: String = ""
    @Published var subtitle: String?
    @Published var message: String?
    @Published var errorText: String?
    @Published var isLoading = true

    private var isFirstAppear = true

    private let transactionId: String
    private let fetchUseCase: FetchTransactionByIdUseCase

    init(transactionId: String, fetchUseCase: FetchTransactionByIdUseCase) {
        self.transactionId = transactionId
        self.fetchUseCase = fetchUseCase
    }

    // MARK: - Input

    func onAppear() {
        if isFirstAppear {
            isFirstAppear.toggle()
            fetch(isReload: false)
        }
    }

    func retry() {
        fetch(isReload: true)
    }

    // MARK: - Private

    private func fetch(isReload: Bool) {

        isLoading = true

        let cachePolicy: CachePolicy = isReload ? .ignore : .useCache

        fetchUseCase.fetch(transactionId: transactionId, cachePolicy: cachePolicy) { [weak self] result in
            guard let self = self else { return }
            DispatchQueue.main.async {
                self.isLoading = false

                switch result {
                case .success(let transaction):
                    self.handle(transaction)
                case .failure(let error):
                    self.handle(error)
                }
            }
        }
    }

    private func handle(_ transaction: Transaction?) {
        errorText = nil

        guard let transaction = transaction else {
            displayNotFoundMessage()
            return
        }

        message = nil
        title = transaction.title
        subtitle = transaction.subtitle
    }

    private func handle(_ error: Error) {
        errorText = "Error happened: " + error.localizedDescription
    }

    private func displayNotFoundMessage() {
        message = "Transaction not found"
    }
}
