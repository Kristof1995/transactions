//
//  SwiftUITransactionDetailView.swift
//  Trial
//
//  Created by Kristof Varga on 2021. 09. 05..
//  Copyright © 2021. BeeOne Gmbh. All rights reserved.
//

import SwiftUI

struct SwiftUITransactionDetailView: View {

    @EnvironmentObject var viewModel: TransactionDetailViewModel

    @State private var isAlertPresented = false

    var body: some View {

        ZStack {
            VStack {
                Text(viewModel.title)
                Text(viewModel.subtitle ?? "")
            }

            if viewModel.isLoading {
                ActivityIndicator(isAnimating: viewModel.isLoading)
            }
        }
        .onAppear {
            viewModel.onAppear()
        }
        .onReceive(viewModel.$errorText, perform: { errorText in
            isAlertPresented = errorText != nil
        })
        .errorAlert(isPresented: $isAlertPresented, message: viewModel.errorText, retry: viewModel.retry)
    }
}

struct SwiftUITransactionDetailView_Previews: PreviewProvider {
    static var previews: some View {

        let fetchUseCase = MockFetchTransactionByIdUseCase()
        fetchUseCase.transaction = Transaction(id: "1", title: "title", subtitle: "subtitle", amount: .init(value: .zero, currency: "EUR"), additionalTexts: [])

        let viewModel = TransactionDetailViewModel(transactionId: "1", fetchUseCase: fetchUseCase)

        return SwiftUITransactionDetailView()
            .environmentObject(viewModel)
    }
}
