//
//  TransactionListViewModelTests.swift
//  TrialTests
//
//  Created by Kristof Varga on 2021. 09. 03..
//  Copyright © 2021. BeeOne Gmbh. All rights reserved.
//

import Foundation
import XCTest
@testable import Trial
import Combine

class TransactionListViewModelTests: XCTestCase {

    

    var sut: TransactionListViewModel!
    var fetchTransactionsUseCase: MockFetchTransactionsUseCase!
    var sumTransactionsUseCase: MockSumTransactionsUseCase!
    var firstLaunchUseCase: MockFirstLaunchUseCase!

    var subscriptions: Set<AnyCancellable> = []

    private let formatter = NumberFormatter()

    override func setUp() {

        formatter.numberStyle = .currency

        let fetchTransactionsUseCase = MockFetchTransactionsUseCase()
        let sumTransactionsUseCase = MockSumTransactionsUseCase()
        let firstLaunchUseCase = MockFirstLaunchUseCase()

        self.fetchTransactionsUseCase = fetchTransactionsUseCase
        self.sumTransactionsUseCase = sumTransactionsUseCase
        self.firstLaunchUseCase = firstLaunchUseCase

        sut = TransactionListViewModel(fetchTransactionsUseCase: fetchTransactionsUseCase, sumTransactionsUseCase: sumTransactionsUseCase, firstLaunchUseCase: firstLaunchUseCase)
    }

    func testSutExists() {
        XCTAssertNotNil(sut)
    }

    func testFirstAppear() {
        let expectation = expectation(description: "\(#function) sink should called")

        fetchTransactionsUseCase.transactions = []

        sut.onAppear()

        sut.$items
            .dropFirst()
            .sink { items in
                expectation.fulfill()
                XCTAssertEqual(items.count, 0)
            }
            .store(in: &subscriptions)

        wait(for: [expectation], timeout: 2.0)
    }

    func testTransactionFetching() {
        let fetchingExpectation = expectation(description: "\(#function) sink fetchingExpectation")
        let loadingExpectation = expectation(description: "\(#function) sink loadingExpectation")

        fetchTransactionsUseCase.transactions = [
            .init(id: "1", title: "title1", subtitle: "subtitle1", amount: .init(value: .zero, currency: "eur"), additionalTexts: ["Additional1", "Additional2"]),

            .init(id: "2", title: "title2", subtitle: nil, amount: .init(value: .zero, currency: "eur"), additionalTexts: []),
        ]

        formatter.currencyCode = "eur"

        sut.onAppear()

        sut.$items
            .dropFirst()
            .receive(on: RunLoop.main)
            .sink { items in
                fetchingExpectation.fulfill()
                XCTAssertEqual(items.count, 2)
                XCTAssertEqual(items.first?.title, "title1")
                XCTAssertEqual(items.first?.subtitle, "subtitle1")
                XCTAssertEqual(items.first?.amount, self.formatter.string(from: 0))
                XCTAssertEqual(items.first?.additionalTexts.count, 2)
                XCTAssertEqual(items.first?.additionalTexts.first, "Additional1")
            }
            .store(in: &subscriptions)

        sut.$isLoading
            .collect(.byTime(DispatchQueue.main, 2.0))
            .sink { isLoadingArray in
                loadingExpectation.fulfill()
                XCTAssertEqual(isLoadingArray.count, 2)
                XCTAssert(isLoadingArray[0])
                XCTAssertFalse(isLoadingArray[1])
            }
            .store(in: &subscriptions)

        wait(for: [fetchingExpectation, loadingExpectation], timeout: 3.0)
    }

    func testError() {
        let fetchingExpectation = expectation(description: "\(#function) sink fetchingExpectation")
        let loadingExpectation = expectation(description: "\(#function) sink loadingExpectation")

        fetchTransactionsUseCase.error = TestError.mockError

        sut.onAppear()

        sut.$errorText
            .dropFirst()
            .receive(on: RunLoop.main)
            .sink { errorText in
                fetchingExpectation.fulfill()
                XCTAssertNotNil(errorText)
                if let errorText = errorText {
                    XCTAssert(errorText.contains(TestError.mockError.localizedDescription))
                }
            }
            .store(in: &subscriptions)

        sut.$isLoading
            .collect(.byTime(DispatchQueue.main, 2.0))
            .sink { isLoadingArray in
                loadingExpectation.fulfill()
                XCTAssertEqual(isLoadingArray.count, 2)
                XCTAssert(isLoadingArray[0])
                XCTAssertFalse(isLoadingArray[1])
            }
            .store(in: &subscriptions)

        wait(for: [fetchingExpectation, loadingExpectation], timeout: 3.0)
    }

    func testRetry() {
        let fetchingExpectation = expectation(description: "\(#function) sink fetchingExpectation")
        let loadingExpectation = expectation(description: "\(#function) sink loadingExpectation")

        fetchTransactionsUseCase.transactions = [
            .init(id: "1", title: "title1", subtitle: "subtitle1", amount: .init(value: .zero, currency: "eur"), additionalTexts: []),

            .init(id: "2", title: "title2", subtitle: nil, amount: .init(value: .zero, currency: "eur"), additionalTexts: []),
        ]

        formatter.currencyCode = "eur"

        sut.retry()

        sut.$items
            .dropFirst()
            .receive(on: RunLoop.main)
            .sink { items in
                fetchingExpectation.fulfill()
                XCTAssertEqual(items.count, 2)
                XCTAssertEqual(items.first?.title, "title1")
                XCTAssertEqual(items.first?.subtitle, "subtitle1")
                XCTAssertEqual(items.first?.amount, self.formatter.string(from: 0))
            }
            .store(in: &subscriptions)

        sut.$isLoading
            .collect(.byTime(DispatchQueue.main, 2.0))
            .sink { isLoadingArray in
                loadingExpectation.fulfill()
                XCTAssertEqual(isLoadingArray.count, 2)
                XCTAssert(isLoadingArray[0])
                XCTAssertFalse(isLoadingArray[1])
            }
            .store(in: &subscriptions)

        wait(for: [fetchingExpectation, loadingExpectation], timeout: 3.0)
    }

    func testSumDisplaying() {
        let expectation = expectation(description: "\(#function) sink completion")

        fetchTransactionsUseCase.transactions = [
            .init(id: "1", title: "title1", subtitle: "subtitle1", amount: .init(value: NSDecimalNumber(value: 1), currency: "eur"), additionalTexts: []),

            .init(id: "2", title: "title2", subtitle: nil, amount: .init(value: NSDecimalNumber(value: 2.2), currency: "eur"), additionalTexts: []),

            .init(id: "3", title: "title3", subtitle: nil, amount: .init(value: NSDecimalNumber(value: 3), currency: "usd"), additionalTexts: []),

            .init(id: "4", title: "title4", subtitle: nil, amount: .init(value: NSDecimalNumber(value: 2.8), currency: "usd"), additionalTexts: []),
        ]

        sumTransactionsUseCase.sumAmounts = [
            .init(value: NSDecimalNumber(value: 3.2), currency: "eur"),
            .init(value: NSDecimalNumber(value: 5.8), currency: "usd")
        ]

        sut.onAppear()

        sut.$balances
            .dropFirst()
            .sink { balances in
                expectation.fulfill()
                XCTAssertEqual(balances.count, 2)

                self.formatter.currencyCode = "eur"
                XCTAssertEqual(balances[0], self.formatter.string(from: 3.2))
                self.formatter.currencyCode = "usd"
                XCTAssertEqual(balances[1], self.formatter.string(from: 5.8))
            }
            .store(in: &subscriptions)

        wait(for: [expectation], timeout: 3.0)
    }

    func testFirstLaunch() {
        let expectation = expectation(description: "\(#function) sink completion")

        firstLaunchUseCase.isFirst = true

        sut.onAppear()

        sut.$isSplashPresented
            .sink { isSplashPresented in
                expectation.fulfill()
                XCTAssert(isSplashPresented)
            }
            .store(in: &subscriptions)

        wait(for: [expectation], timeout: 1.0)
    }

    func testNonFirstLaunch() {
        let expectation = expectation(description: "\(#function) sink completion")

        firstLaunchUseCase.isFirst = false

        sut.onAppear()

        sut.$isSplashPresented
            .sink { isSplashPresented in
                expectation.fulfill()
                XCTAssertFalse(isSplashPresented)
            }
            .store(in: &subscriptions)

        wait(for: [expectation], timeout: 1.0)
    }

    func testSavingFirstLaunch() {
        firstLaunchUseCase.isFirst = true

        sut.onAppear()

        XCTAssertFalse(firstLaunchUseCase.isFirst)
    }
}
