//
//  TransactionDetailViewModelTests.swift
//  TrialTests
//
//  Created by Kristof Varga on 2021. 09. 05..
//  Copyright © 2021. BeeOne Gmbh. All rights reserved.
//

import XCTest
import Combine
@testable import Trial

class TransactionDetailViewModelTests: XCTestCase {

    private let transactionId = "1"

    var sut: TransactionDetailViewModel!
    var fetchUseCase: MockFetchTransactionByIdUseCase!

    var subscriptions: Set<AnyCancellable> = []

    override func setUp() {
        let fetchUseCase = MockFetchTransactionByIdUseCase()
        self.fetchUseCase = fetchUseCase
        sut = TransactionDetailViewModel(transactionId: transactionId, fetchUseCase: fetchUseCase)
    }

    func sutExists() {
        XCTAssertNotNil(sut)
    }

    func testNotFound() {
        let expectation = expectation(description: "\(#function) sink should called")

        fetchUseCase.transaction = nil
        fetchUseCase.error = nil

        sut.onAppear()

        sut.$message
            .dropFirst()
            .sink { message in
                expectation.fulfill()
                XCTAssertNotNil(message)
                XCTAssertEqual(message, "Transaction not found")
            }
            .store(in: &subscriptions)

        wait(for: [expectation], timeout: 2.0)
    }

    func testTransactionFound() {
        let messageExpectation = expectation(description: "\(#function) message sink should called")

        let titleExpectation = expectation(description: "\(#function) title sink should called")

        let subtitleExpectation = expectation(description: "\(#function) subtitle sink should called")

        fetchUseCase.transaction = .init(id: "1", title: "title", subtitle: "subtitle", amount: Transaction.Amount.init(value: .zero, currency: "EUR"), additionalTexts: [])

        sut.onAppear()

        sut.$message
            .dropFirst()
            .sink { message in
                messageExpectation.fulfill()
                XCTAssertNil(message)
            }
            .store(in: &subscriptions)

        sut.$title
            .dropFirst()
            .sink { title in
                titleExpectation.fulfill()
                XCTAssertEqual(title, "title")
            }
            .store(in: &subscriptions)

        sut.$subtitle
            .dropFirst()
            .sink { subtitle in
                subtitleExpectation.fulfill()
                XCTAssertEqual(subtitle, "subtitle")
            }
            .store(in: &subscriptions)

        wait(for: [messageExpectation, titleExpectation, subtitleExpectation], timeout: 2.0)
    }

    func testError() {
        let expectation = expectation(description: "\(#function) sink should called")

        fetchUseCase.error = TestError.mockError

        sut.onAppear()

        sut.$errorText
            .dropFirst()
            .sink { errorText in
                expectation.fulfill()
                XCTAssertNotNil(errorText)
            }
            .store(in: &subscriptions)

        wait(for: [expectation], timeout: 2.0)
    }

    func testLoading() {
        let expectation = expectation(description: "\(#function) sink should called")

        fetchUseCase.error = TestError.mockError

        sut.retry()

        sut.$isLoading
            .collect(.byTime(DispatchQueue.main, 2.0))
            .sink { isLoadingArray in
                expectation.fulfill()
                XCTAssertEqual(isLoadingArray.count, 2)
                XCTAssert(isLoadingArray[0])
                XCTAssertFalse(isLoadingArray[1])
            }
            .store(in: &subscriptions)

        wait(for: [expectation], timeout: 2.0)
    }
}
