//
//  TrialTests.swift
//  TrialTests
//
//  Created by Stefan Rinner on 12.02.18.
//  Copyright © 2018 BeeOne Gmbh. All rights reserved.
//

import XCTest
@testable import Trial

class TrialTests: XCTestCase {
    
    func testTransactionParsing() {
        let api = TransactionsAPI()

        let expectation = XCTestExpectation(description: "Load transactions")
        
        api.loadTransactions { result in
            switch result {
            case .success(let transactions):
                XCTAssertEqual(45, transactions.count)
            case .failure(_):
                break
            }
            expectation.fulfill()
        }
        
        wait(for: [expectation], timeout: 10.0)
    }

    func testTransactionAPIConformsTransactionSercvice() {
        let api: TransactionService = TransactionsAPI()

        let expectation = XCTestExpectation(description: "Load transactions")

        api.fetchTransactionDTOs { result in
            switch result {
            case .success(let transactions):
                XCTAssertEqual(45, transactions.count)
            case .failure(_):
                break
            }
            expectation.fulfill()
        }

        wait(for: [expectation], timeout: 10.0)
    }
}
