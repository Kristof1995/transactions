//
//  FetchTransactionByIdUseCaseTests.swift
//  TrialTests
//
//  Created by Kristof Varga on 2021. 09. 02..
//  Copyright © 2021. BeeOne Gmbh. All rights reserved.
//

import XCTest
@testable import Trial

class FetchTransactionByIdUseCaseTests: XCTestCase {

    var transactionRepository: MockTransactionRepository!

    var sut: DefaultFetchTransactionByIdUseCase!

    override func setUp() {
        let transactionRepository = MockTransactionRepository()
        self.transactionRepository = transactionRepository
        self.sut = DefaultFetchTransactionByIdUseCase(transactionRepository: transactionRepository)
    }

    func testUseCaseExists() {
        XCTAssertNotNil(sut)
    }

    func testEmpty_shouldBeNil() {
        let expectation = expectation(description: "\(#function) completion should called")

        transactionRepository.transactions = []

        sut.fetch(transactionId: "1", cachePolicy: .ignore) { result in
            expectation.fulfill()

            switch result {
            case .success(let transaction):
                XCTAssertNil(transaction)
            case .failure(_):
                XCTFail("should succeed")
            }
        }

        wait(for: [expectation], timeout: 1.0)
    }

    func testError() {
        let expectation = expectation(description: "\(#function) completion should called")

        transactionRepository.error = TestError.mockError

        sut.fetch(transactionId: "1", cachePolicy: .ignore) { result in
            expectation.fulfill()

            switch result {
            case .success(_):
                XCTFail("should fail")
            case .failure(let error):
                XCTAssertEqual(error as! TestError, TestError.mockError)
            }
        }

        wait(for: [expectation], timeout: 1.0)
    }

    func testNotExists() {
        let expectation = expectation(description: "\(#function) completion should called")

        transactionRepository.transactions = [
            .init(id: "2", title: "", subtitle: nil, amount: .init(value: .zero, currency: ""), additionalTexts: [])
        ]

        sut.fetch(transactionId: "1", cachePolicy: .ignore) { result in
            expectation.fulfill()

            switch result {
            case .success(let transaction):
                XCTAssertNil(transaction)
            case .failure(_):
                XCTFail("should succeed")
            }
        }

        wait(for: [expectation], timeout: 1.0)
    }

    func testExists_OnlyOneTransaction() {
        let expectation = expectation(description: "\(#function) completion should called")

        transactionRepository.transactions = [
            .init(id: "1", title: "", subtitle: nil, amount: .init(value: .zero, currency: ""), additionalTexts: [])
        ]



        sut.fetch(transactionId: "1", cachePolicy: .ignore) { result in
            expectation.fulfill()

            switch result {
            case .success(let transaction):
                XCTAssertNotNil(transaction)
                XCTAssertEqual(transaction?.id, "1")
            case .failure(_):
                XCTFail("should succeed")
            }
        }

        wait(for: [expectation], timeout: 1.0)
    }

    func testExists_MultipleTransactions() {
        let expectation = expectation(description: "\(#function) completion should called")

        transactionRepository.transactions = [
            .init(id: "1", title: "a", subtitle: nil, amount: .init(value: .zero, currency: ""), additionalTexts: []),
            .init(id: "2", title: "b", subtitle: nil, amount: .init(value: .zero, currency: ""), additionalTexts: []),
            .init(id: "3", title: "c", subtitle: nil, amount: .init(value: .zero, currency: ""), additionalTexts: [])
        ]

        sut.fetch(transactionId: "1", cachePolicy: .ignore) { result in
            expectation.fulfill()

            switch result {
            case .success(let transaction):
                XCTAssertNotNil(transaction)
                XCTAssertEqual(transaction?.id, "1")
            case .failure(_):
                XCTFail("should succeed")
            }
        }

        wait(for: [expectation], timeout: 1.0)
    }
}
