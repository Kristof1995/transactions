//
//  DefaultFirstLaunchUsecaseTests.swift
//  TrialTests
//
//  Created by Kristof Varga on 2021. 09. 06..
//  Copyright © 2021. BeeOne Gmbh. All rights reserved.
//

import Foundation
import XCTest
@testable import Trial

class DefaultFirstLaunchUsecaseTests: XCTestCase {

    class MockPreferences: UserPreferencesRepository {

        var value: Any?

        func getValue<T>(for key: String) -> T? {
            value as? T
        }

        func save<T>(_ value: T, for key: String) {
            self.value = value
        }
    }

    var sut: DefaultFirstLaunchUseCase!
    var userPreferences: MockPreferences!

    override func setUp() {
        let userPreferences = MockPreferences()
        self.userPreferences = userPreferences
        sut = DefaultFirstLaunchUseCase(userPreferences: userPreferences)
    }

    func testFirstLaunchTrue() {
        userPreferences.value = true

        XCTAssert(sut.isFirstLaunch())
    }

    func testFirstLaunchFalse() {
        userPreferences.value = false

        XCTAssertFalse(sut.isFirstLaunch())
    }

    func testFirstLaunchValueNotExists() {
        userPreferences.value = nil

        XCTAssert(sut.isFirstLaunch())
    }

    func testSave() {
        userPreferences.value = nil

        sut.saveFirtsLaunch()

        XCTAssertFalse(sut.isFirstLaunch())
    }
}
