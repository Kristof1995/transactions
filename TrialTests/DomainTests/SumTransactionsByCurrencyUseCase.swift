//
//  SumTransactionsByCurrencyUseCase.swift
//  TrialTests
//
//  Created by Kristof Varga on 2021. 09. 02..
//  Copyright © 2021. BeeOne Gmbh. All rights reserved.
//

import XCTest
@testable import Trial

class SumTransactionsByCurrencyUseCase: XCTestCase {

    var sut: DefaultSumTransactionsByCurrencyUseCase!

    override func setUp() {
        self.sut = DefaultSumTransactionsByCurrencyUseCase()
    }

    func testSutExists() {
        XCTAssertNotNil(sut)
    }

    func testEmpty() {
        let sumAmounts = sut.calculeteSumGroupedByCurrency(transactions: [])
        XCTAssertEqual(sumAmounts.count, 0)
    }

    func testOneWithZeroAmount() {
        let sumAmounts = sut.calculeteSumGroupedByCurrency(transactions: [
            .init(id: "1", title: "title", subtitle: nil, amount: .init(value: .zero, currency: "eur"), additionalTexts: [])
        ])

        XCTAssertEqual(sumAmounts.count, 1)
        XCTAssertEqual(sumAmounts.first?.value, 0)
        XCTAssertEqual(sumAmounts.first?.currency, "eur")
    }

    func testOneWithSomeAmount() {
        let sumAmounts = sut.calculeteSumGroupedByCurrency(transactions: [
            .init(id: "1", title: "title", subtitle: nil, amount: .init(value: NSDecimalNumber(1.2345), currency: "eur"), additionalTexts: [])
        ])

        XCTAssertEqual(sumAmounts.count, 1)
        XCTAssertEqual(sumAmounts.first?.value, 1.2345)
        XCTAssertEqual(sumAmounts.first?.currency, "eur")
    }

    func testOneWithMultipleAmount() {
        let sumAmounts = sut.calculeteSumGroupedByCurrency(transactions: [
            .init(id: "1", title: "title", subtitle: nil, amount: .init(value: NSDecimalNumber(1.0), currency: "eur"), additionalTexts: []),
            .init(id: "2", title: "titl2", subtitle: nil, amount: .init(value: NSDecimalNumber(2.0), currency: "eur"), additionalTexts: []),
            .init(id: "3", title: "titl3", subtitle: nil, amount: .init(value: NSDecimalNumber(0.16), currency: "eur"), additionalTexts: [])
        ])

        XCTAssertEqual(sumAmounts.count, 1)
        XCTAssertEqual(sumAmounts.first?.value, 3.16)
        XCTAssertEqual(sumAmounts.first?.currency, "eur")
    }

    func testDifferentCurrencies() {
        let sumAmounts = sut.calculeteSumGroupedByCurrency(transactions: [
            .init(id: "1", title: "title", subtitle: nil, amount: .init(value: NSDecimalNumber(1.0), currency: "eur"), additionalTexts: []),
            .init(id: "2", title: "titl2", subtitle: nil, amount: .init(value: NSDecimalNumber(2.0), currency: "huf"), additionalTexts: []),
            .init(id: "3", title: "titl3", subtitle: nil, amount: .init(value: NSDecimalNumber(0.16), currency: "usd"), additionalTexts: [])
        ])

        XCTAssertEqual(sumAmounts.count, 3)

        XCTAssertEqual(sumAmounts[0].value, 1)
        XCTAssertEqual(sumAmounts[0].currency, "eur")

        XCTAssertEqual(sumAmounts[1].value, 2)
        XCTAssertEqual(sumAmounts[1].currency, "huf")

        XCTAssertEqual(sumAmounts[2].value, 0.16)
        XCTAssertEqual(sumAmounts[2].currency, "usd")
    }
}
