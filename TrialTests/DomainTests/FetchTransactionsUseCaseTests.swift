//
//  FetchTransactionsUseCaseTests.swift
//  TrialTests
//
//  Created by Kristof Varga on 2021. 09. 02..
//  Copyright © 2021. BeeOne Gmbh. All rights reserved.
//

import XCTest
@testable import Trial

class FetchTransactionsUseCaseTests: XCTestCase {

    var transactionRepository: MockTransactionRepository!

    var sut: DefaultFetchTransactionsUseCase!

    override func setUp() {
        let transactionRepository = MockTransactionRepository()
        self.transactionRepository = transactionRepository
        self.sut = DefaultFetchTransactionsUseCase(transactionRepository: transactionRepository)
    }

    func testUseCaseExists() {
        XCTAssertNotNil(sut)
    }

    func testEmpty() {
        let expectation = expectation(description: "\(#function) completion should called")

        transactionRepository.transactions = []

        sut.fetch(cachePolicy: .ignore) { result in
            expectation.fulfill()

            switch result {
            case .success(let transactions):
                XCTAssertEqual(transactions.count, 0)
            case .failure(_):
                XCTFail("should succeed")
            }
        }

        wait(for: [expectation], timeout: 1.0)
    }

    func testError() {
        let expectation = expectation(description: "\(#function) completion should called")

        transactionRepository.error = TestError.mockError

        sut.fetch(cachePolicy: .ignore) { result in
            expectation.fulfill()

            switch result {
            case .success(_):
                XCTFail("should fail")
            case .failure(let error):
                XCTAssertEqual(error as! TestError, TestError.mockError)
            }
        }

        wait(for: [expectation], timeout: 1.0)
    }

    func testTransactions() {
        let expectation = expectation(description: "\(#function) completion should called")

        transactionRepository.transactions = [
            .init(id: "1", title: "title", subtitle: "subtitle", amount: .init(value: .zero, currency: ""), additionalTexts: [])
        ]

        sut.fetch(cachePolicy: .ignore) { result in
            expectation.fulfill()

            switch result {
            case .success(let transactions):
                XCTAssertEqual(transactions.count, 1)
                XCTAssertEqual(transactions.first?.id, "1")
                XCTAssertEqual(transactions.first?.title, "title")
                XCTAssertEqual(transactions.first?.subtitle, "subtitle")
            case .failure(_):
                XCTFail("should succeed")
            }
        }

        wait(for: [expectation], timeout: 1.0)
    }
}
