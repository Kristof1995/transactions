//
//  DefaultTransactionRepositoryTests.swift
//  TrialTests
//
//  Created by Kristof Varga on 2021. 09. 02..
//  Copyright © 2021. BeeOne Gmbh. All rights reserved.
//

import XCTest
@testable import Trial

class DefaultTransactionRepositoryTests: XCTestCase {

    var sut: DefaultTransactionRepository!

    var transactionService: MockTransactionService!

    override func setUp() {
        let transactionService = MockTransactionService()
        self.transactionService = transactionService
        sut = DefaultTransactionRepository(transactionService: transactionService)
    }

    func testRepositoryExists() {
        XCTAssertNotNil(sut)
        XCTAssertNotNil(sut as TransactionRepository)
    }

    func testEmpty() {
        let expectation = expectation(description: "\(#function) completion should called")

        transactionService.transactions = []

        sut.fetchTransactionList(cachePolicy: .ignore) { result in
            expectation.fulfill()

            switch result {
            case .success(let transactions):
                XCTAssertNotNil(transactions)
                XCTAssertEqual(transactions.count, 0)
            case .failure(_):
                XCTFail("should succeed")
            }
        }

        wait(for: [expectation], timeout: 1.0)
    }

    func testError() {
        let expectation = expectation(description: "\(#function) completion should called")

        transactionService.error = TestError.mockError

        sut.fetchTransactionList(cachePolicy: .ignore) { result in
            expectation.fulfill()

            switch result {
            case .success(_):
                XCTFail("should fail")
            case .failure(let error):
                XCTAssertEqual(error as! TestError, TestError.mockError)
            }
        }

        wait(for: [expectation], timeout: 1.0)
    }

    func testOneDTO() {
        let expectation = expectation(description: "\(#function) completion should called")

        transactionService.transactions = [
            createTransactionDto(id: "1", title: "title", subtitle: nil, amount: .init(value: 0, precision: 0, currency: ""))
        ]

        sut.fetchTransactionList(cachePolicy: .ignore) { result in
            expectation.fulfill()

            switch result {
            case .success(let domainModels):
                XCTAssertNotNil(domainModels)
                XCTAssertEqual(domainModels.count, 1)
                XCTAssertEqual(domainModels.first?.id, "1")
            case .failure(_):
                XCTFail("should succeed")
            }
        }

        wait(for: [expectation], timeout: 1.0)
    }

    func testMultipleDTO() {
        let expectation = expectation(description: "\(#function) completion should called")

        transactionService.transactions = [
            createTransactionDto(id: "1", title: "title", subtitle: nil, amount: .init(value: 0, precision: 0, currency: "")),

            createTransactionDto(id: "2", title: "title2", subtitle: nil, amount: .init(value: 1, precision: 1, currency: "huf"))
        ]

        sut.fetchTransactionList(cachePolicy: .ignore) { result in
            expectation.fulfill()

            switch result {
            case .success(let domainModels):
                XCTAssertNotNil(domainModels)
                XCTAssertEqual(domainModels.count, 2)
                XCTAssertEqual(domainModels[0].id, "1")
                XCTAssertEqual(domainModels[1].id, "2")
            case .failure(_):
                XCTFail("should succeed")
            }
        }

        wait(for: [expectation], timeout: 1.0)
    }

    func testCache() {
        let expectation = expectation(description: "\(#function) completion should called")

        transactionService.transactions = [
            createTransactionDto(id: "1", title: "title", subtitle: nil, amount: .init(value: 0, precision: 0, currency: ""))
        ]

        // First fetch
        sut.fetchTransactionList(cachePolicy: .ignore) { _ in
            self.transactionService.transactions = [
                self.createTransactionDto(id: "2", title: "", subtitle: nil, amount: .init(value: 0, precision: 0, currency: ""))
            ]

            // Second fetch from cache
            self.sut.fetchTransactionList(cachePolicy: .useCache) { result in
                expectation.fulfill()
                switch result {
                case .success(let domainModels):
                    XCTAssertNotNil(domainModels)
                    XCTAssertEqual(domainModels.count, 1)
                    XCTAssertEqual(domainModels[0].id, "1")
                case .failure(_):
                    XCTFail("should succeed")
                }
            }
        }

        wait(for: [expectation], timeout: 1.0)
    }

    private func createTransactionDto(id: String, title: String, subtitle: String?, amount: Amount) -> TransactionDTO {
        .init(id: id, title: title, subtitle: subtitle, sender: emptyAccountNumber(), senderName: nil, senderOriginator: nil, senderReference: "", senderBankReference: nil, receiver: emptyAccountNumber(), receiverName: nil, receiverReference: "", creditorId: "", amount: amount, amountSender: amount, bookingDate: Date(), valuationDate: Date(), importDate: Date(), dueDate: nil, exchangeDate: nil, insertTimestamp: Date(), reference: "", originatorSystem: "", additionalTexts: emptyAdditionalTexts(), note: nil, bookingType: "", bookingTypeTranslation: nil, orderRole: "", orderCategory: nil, cardId: "", maskedCardNumber: "", invoiceId: nil, location: "", partnerName: nil, partnerOriginator: nil, partnerAddress: [])
    }

    private func emptyAccountNumber() -> AccountNumber {
        .init(iban: nil, bic: nil, number: nil, bankCode: nil, prefix: nil, countryCode: nil)
    }

    private func emptyAdditionalTexts() -> AdditionalTexts {
        .init(text1: "", text2: "", text3: "", lineItems: [], constantSymbol: nil, variableSymbol: nil, specificSymbol: nil)
    }
}
