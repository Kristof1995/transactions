//
//  MockMockTransactionRepository.swift
//  TrialTests
//
//  Created by Kristof Varga on 2021. 09. 02..
//  Copyright © 2021. BeeOne Gmbh. All rights reserved.
//

import Foundation
import XCTest
@testable import Trial

class MockTransactionRepository: TransactionRepository {
        var transactions: [Transaction]?
        var error: Error?

        func fetchTransactionList(cachePolicy: CachePolicy, completion: @escaping (Result<[Transaction], Error>) -> Void) {
            if let transactions = transactions {
                completion(.success(transactions))
            } else if let error = error {
                completion(.failure(error))
            } else {
                XCTFail("setup MockTransactionRepository's properties")
            }
        }
    }
