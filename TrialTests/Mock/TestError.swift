//
//  TestError.swift
//  TrialTests
//
//  Created by Kristof Varga on 2021. 09. 02..
//  Copyright © 2021. BeeOne Gmbh. All rights reserved.
//

import Foundation

enum TestError: Error {
    case mockError
}
