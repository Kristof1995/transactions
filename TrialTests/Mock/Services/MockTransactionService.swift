//
//  MockTransactionService.swift
//  TrialTests
//
//  Created by Kristof Varga on 2021. 09. 02..
//  Copyright © 2021. BeeOne Gmbh. All rights reserved.
//

import Foundation
import XCTest
@testable import Trial

class MockTransactionService: TransactionService {
    var transactions: [TransactionDTO]?
    var error: Error?

    func fetchTransactionDTOs(completion: @escaping (Result<[TransactionDTO], Error>) -> Void) {
        if let transactions = transactions {
            completion(.success(transactions))
        } else if let error = error {
            completion(.failure(error))
        } else {
            XCTFail("setup MockTransactionService's properties")
        }
    }
}
